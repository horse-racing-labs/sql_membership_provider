defmodule SqlMembershipProvider.MixProject do
  use Mix.Project

  def project do
    [
      app: :sql_membership_provider,
      version: "0.9.2",
      elixir: "~> 1.14",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      docs: docs()
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp description do
    """
    Read and authenticate against ASP.NET Membership Microsoft SQL Server databases
    """
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Eric Lathrop"],
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/horse-racing-labs/sql_membership_provider"},
      source_url: "https://gitlab.com/horse-racing-labs/sql_membership_provider",
      homepage_url: "https://gitlab.com/horse-racing-labs/sql_membership_provider"
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:ecto, "~> 3.7"},
      {:ecto_sql, "~> 3.7"},
      {:ex_doc, "~> 0.25", only: :dev, runtime: false},
      {:mix_test_watch, "~> 1.1", only: :dev, runtime: false},
      {:tds, "~> 2.3.5"},
      {:typed_ecto_schema, "~>  0.3"}
    ]
  end

  defp docs do
    [
      main: "readme",
      extras: ["CHANGELOG.md", "README.md"]
    ]
  end
end
