defmodule SqlMembershipProvider.ApplicationTest do
  use ExUnit.Case, async: true

  alias SqlMembershipProvider.Application
  import SqlMembershipProvider.Application

  setup context do
    if context[:integration] do
      :ok = Ecto.Adapters.SQL.Sandbox.checkout(TestRepo)
    end

    :ok
  end

  describe "find_by_application_id/1" do
    @tag :integration
    test "with unknown application returns nil" do
      assert find_by_application_id("11111111-1111-1111-1111-111111111111") |> TestRepo.one() ==
               nil
    end

    @tag :integration
    test "with existing application returns application" do
      assert %Application{
               application_name: "Sample",
               lowered_application_name: "sample"
             } = find_by_application_id("5dd7c210-3342-477b-ade9-d89b3255e1e9") |> TestRepo.one!()
    end
  end

  describe "find_by_application_name/1" do
    @tag :integration
    test "with unknown application returns nil" do
      assert find_by_application_name("unknown") |> TestRepo.one() == nil
    end

    @tag :integration
    test "with existing application returns application" do
      assert %Application{
               application_name: "Sample",
               lowered_application_name: "sample"
             } = find_by_application_name("Sample") |> TestRepo.one!()
    end
  end
end
