defmodule SqlMembershipProvider.RoleTest do
  use ExUnit.Case, async: true

  alias SqlMembershipProvider.Role
  import SqlMembershipProvider.Role

  setup context do
    if context[:integration] do
      :ok = Ecto.Adapters.SQL.Sandbox.checkout(TestRepo)
    end

    :ok
  end

  describe "find_by_user_id/1" do
    @tag :integration
    test "with unknown user returns empty list" do
      assert find_by_user_id("00000000-0000-0000-0000-000000000000") |> TestRepo.all() == []
    end

    @tag :integration
    test "with existing user returns profile" do
      assert [
               %Role{
                 role_name: "Administrators"
               }
             ] = find_by_user_id("1557b0fd-40ad-4c6e-9651-161136ad68d2") |> TestRepo.all()
    end
  end
end
