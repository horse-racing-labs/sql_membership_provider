defmodule SqlMembershipProvider.ProfileTest do
  use ExUnit.Case, async: true

  alias SqlMembershipProvider.Profile
  import SqlMembershipProvider.Profile

  setup context do
    if context[:integration] do
      :ok = Ecto.Adapters.SQL.Sandbox.checkout(TestRepo)
    end

    :ok
  end

  describe "create_changeset/2" do
    @tag :integration
    test "with required_params, creates a new profile" do
      SqlMembershipProvider.User.create_changeset(
        %SqlMembershipProvider.User{},
        SqlMembershipProvider.UserTest.new_user_params("Test User")
      )
      |> TestRepo.insert!()

      assert %Profile{
               property_names:
                 "AgreedToTerms:S:0:4:AgreedToTermsPhoto:S:4:5:BirthDate:S:9:80:FirstName:S:89:4:LastIPAddress:S:93:10:LastName:S:103:3:PostalCode:S:106:5:Sex:S:111:1:SilkImageGuid:S:112:36:",
               property_values_string:
                 "TrueFalse<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<dateTime>1969-10-05T00:00:00</dateTime>John10.4.0.245Doe71111M82a6f74e-9ac9-40ce-87c2-3534f78ce27d"
             } =
               create_changeset(%Profile{}, new_profile_params())
               |> TestRepo.insert!()
    end
  end

  describe "generate_properties/1" do
    @tag :integration
    test "given params, returns proprty_names and values" do
      assert %{
               "property_names" =>
                 "AgreedToTerms:S:0:4:AgreedToTermsPhoto:S:4:5:BirthDate:S:9:80:FirstName:S:89:4:LastIPAddress:S:93:10:LastName:S:103:3:PostalCode:S:106:5:Sex:S:111:1:SilkImageGuid:S:112:36:",
               "property_values_string" =>
                 "TrueFalse<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<dateTime>1969-10-05T00:00:00</dateTime>John10.4.0.245Doe71111M82a6f74e-9ac9-40ce-87c2-3534f78ce27d"
             } =
               generate_properties(%{
                 "AgreedToTerms" => "True",
                 "AgreedToTermsPhoto" => "False",
                 "BirthDate" =>
                   "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<dateTime>1969-10-05T00:00:00</dateTime>",
                 "FirstName" => "John",
                 "LastIPAddress" => "10.4.0.245",
                 "LastName" => "Doe",
                 "PostalCode" => "71111",
                 "Sex" => "M",
                 "SilkImageGuid" => "82a6f74e-9ac9-40ce-87c2-3534f78ce27d"
               })
    end
  end

  describe "find_by_user_id/1" do
    @tag :integration
    test "with unknown user returns nil" do
      assert find_by_user_id("00000000-0000-0000-0000-000000000000") |> TestRepo.one() == nil
    end

    @tag :integration
    test "with existing user returns profile" do
      assert %Profile{
               user_id: "1557b0fd-40ad-4c6e-9651-161136ad68d2"
             } = find_by_user_id("1557b0fd-40ad-4c6e-9651-161136ad68d2") |> TestRepo.one!()
    end
  end

  describe "properties/1" do
    test "with string properties returns map of parsed properties" do
      profile = %Profile{
        property_names:
          "PostalCode:S:0:5:AgreedToTerms:S:5:4:AgreedToTermsPhoto:S:9:5:SilkImageGuid:S:14:36:LastIPAddress:S:50:10:LastName:S:60:3:BirthDate:S:63:81:FirstName:S:144:4:Sex:S:148:1:",
        property_values_string:
          "71111TrueFalse82a6f74e-9ac9-40ce-87c2-3534f78ce27d10.4.0.245Doe<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<dateTime>1969-10-05T00:00:00</dateTime>JohnM"
      }

      assert properties(profile) == %{
               "AgreedToTerms" => "True",
               "AgreedToTermsPhoto" => "False",
               "BirthDate" =>
                 "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<dateTime>1969-10-05T00:00:00</dateTime>",
               "FirstName" => "John",
               "LastIPAddress" => "10.4.0.245",
               "LastName" => "Doe",
               "PostalCode" => "71111",
               "Sex" => "M",
               "SilkImageGuid" => "82a6f74e-9ac9-40ce-87c2-3534f78ce27d"
             }
    end

    test "with reference type property returns map with nil value" do
      profile = %Profile{
        property_names: "SomeReference:S:0:-1:"
      }

      assert properties(profile) == %{"SomeReference" => nil}
    end

    test "with binary property returns map with nil value" do
      profile = %Profile{
        property_names: "SomeBinary:B:0:1:"
      }

      assert properties(profile) == %{"SomeBinary" => nil}
    end
  end

  def new_profile_params() do
    %{
      "AgreedToTerms" => "True",
      "AgreedToTermsPhoto" => "False",
      "BirthDate" =>
        "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<dateTime>1969-10-05T00:00:00</dateTime>",
      "FirstName" => "John",
      "LastIPAddress" => "10.4.0.245",
      "LastName" => "Doe",
      "PostalCode" => "71111",
      "Sex" => "M",
      "SilkImageGuid" => "82a6f74e-9ac9-40ce-87c2-3534f78ce27d",
      "user_id" => "34511dc0-9051-451a-b9f1-c9fabdac0e49"
    }
  end
end
