defmodule SqlMembershipProvider.MembershipTest do
  use ExUnit.Case, async: true

  alias SqlMembershipProvider.Membership
  import SqlMembershipProvider.Membership

  setup context do
    if context[:integration] do
      :ok = Ecto.Adapters.SQL.Sandbox.checkout(TestRepo)
    end

    :ok
  end

  @sample_membership_password "password!"

  describe "find_by_user_id/1" do
    @tag :integration
    test "with unknown user returns nil" do
      assert find_by_user_id("00000000-0000-0000-0000-000000000000") |> TestRepo.one() == nil
    end

    @tag :integration
    test "with existing user returns membership" do
      assert %Membership{
               user_id: "1557b0fd-40ad-4c6e-9651-161136ad68d2"
             } = find_by_user_id("1557b0fd-40ad-4c6e-9651-161136ad68d2") |> TestRepo.one!()
    end
  end

  describe "find_by_email/2" do
    @tag :integration
    test "with unknown user returns nil" do
      assert find_by_email("notanemail", "Sample") |> TestRepo.one() == nil
    end

    @tag :integration
    test "with existing user returns membership" do
      assert %Membership{
               user_id: "1557b0fd-40ad-4c6e-9651-161136ad68d2"
             } = find_by_email("johndoe@example.com", "Sample") |> TestRepo.one!()
    end

    @tag :integration
    test "with unknown application_name returns nil" do
      assert find_by_email("johndoe@example.com", "unknown") |> TestRepo.one() == nil
    end
  end

  describe "is_password_valid?/1" do
    test "with invalid password returns false" do
      assert is_password_valid?(sample_membership(), "invalid") == false
    end

    test "with valid password returns true" do
      assert is_password_valid?(sample_membership(), @sample_membership_password) == true
    end

    test "with unsupported password format raises" do
      assert_raise SqlMembershipProvider.UnsupportedPasswordFormatError,
                   "Unsupported password format: -1",
                   fn ->
                     membership = %{sample_membership() | password_format: -1}
                     is_password_valid?(membership, @sample_membership_password)
                   end
    end
  end

  describe "create_changeset/2" do
    @tag :integration
    test "after creating a membership, it's possible to validate the password" do
      membership =
        create_changeset(%Membership{}, new_membership_params("test_password"))
        |> TestRepo.insert!()

      assert is_password_valid?(membership, "test_password")
    end
  end

  describe "password_update_changeset/2" do
    @tag :integration
    test "after calling password update, new password validates" do
      membership =
        create_changeset(%Membership{}, new_membership_params("test_password"))
        |> TestRepo.insert!()
        |> password_update_changeset(%{
          plaintext_password: "test_password",
          new_password: "updated_password"
        })
        |> TestRepo.update!()

      assert is_password_valid?(membership, "updated_password")
    end

    @tag :integration
    test "attempting a password update with an incorrect password returns an errored changeset" do
      changeset =
        create_changeset(%Membership{}, new_membership_params("test_password"))
        |> TestRepo.insert!()
        |> password_update_changeset(%{
          plaintext_password: "wrong_password",
          new_password: "updated_password"
        })

      assert {:error, _response} = TestRepo.update(changeset)
    end
  end

  describe "password_reset_changeset/2" do
    @tag :integration
    test "after calling password update, new password validates" do
      membership =
        create_changeset(%Membership{}, new_membership_params("test_password"))
        |> TestRepo.insert!()
        |> password_reset_changeset(%{
          new_password: "updated_password"
        })
        |> TestRepo.update!()

      assert is_password_valid?(membership, "updated_password")
    end
  end

  defp new_membership_params(password) do
    %{
      "application_id" => "5dd7c210-3342-477b-ade9-d89b3255e1e9",
      "email" => "Email@address.com",
      "plaintext_password" => password,
      "is_approved" => true,
      "is_locked_out" => false,
      "user_id" => "34511dc0-9051-451a-b9f1-c9fabdac0e49"
    }
  end

  defp sample_membership() do
    %Membership{
      password: "4n+nVVpHIrwYQegAAB9HROz01Bg=",
      password_format: 1,
      password_salt: "UX3qecrwZlIhkjlOTMu3dQ=="
    }
  end
end
