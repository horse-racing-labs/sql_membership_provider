defmodule SqlMembershipProvider.UserTest do
  use ExUnit.Case, async: true

  alias SqlMembershipProvider.User
  import SqlMembershipProvider.User

  setup context do
    if context[:integration] do
      :ok = Ecto.Adapters.SQL.Sandbox.checkout(TestRepo)
    end

    :ok
  end

  describe "create_changeset/2" do
    @tag :integration
    test "with required_params, creates a new user" do
      assert %User{lowered_user_name: "test user"} =
               create_changeset(
                 %User{},
                 new_user_params("Test User")
               )
               |> TestRepo.insert!()
    end
  end

  describe "find_by_user_id/1" do
    @tag :integration
    test "with unknown user returns nil" do
      assert find_by_user_id("11111111-1111-1111-1111-111111111111") |> TestRepo.one() == nil
    end

    @tag :integration
    test "with existing user returns user" do
      assert %User{
               lowered_user_name: "johndoe",
               membership: %SqlMembershipProvider.Membership{email: "johndoe@example.com"},
               profile: %SqlMembershipProvider.Profile{},
               roles: [
                 %SqlMembershipProvider.Role{role_name: "Administrators"}
               ],
               user_id: "1557b0fd-40ad-4c6e-9651-161136ad68d2",
               user_name: "JohnDoe"
             } = find_by_user_id("1557b0fd-40ad-4c6e-9651-161136ad68d2") |> TestRepo.one!()
    end
  end

  describe "find_by_user_name/2" do
    @tag :integration
    test "with unknown user returns nil" do
      assert find_by_user_name("unknown", "Sample") |> TestRepo.one() == nil
    end

    @tag :integration
    test "with unknown application_name returns nil" do
      assert find_by_user_name("JohnDoe", "unknown") |> TestRepo.one() == nil
    end

    @tag :integration
    test "with existing user returns user" do
      assert %User{
               lowered_user_name: "johndoe",
               membership: %SqlMembershipProvider.Membership{email: "johndoe@example.com"},
               profile: %SqlMembershipProvider.Profile{},
               roles: [
                 %SqlMembershipProvider.Role{role_name: "Administrators"}
               ],
               user_id: "1557b0fd-40ad-4c6e-9651-161136ad68d2",
               user_name: "JohnDoe"
             } = find_by_user_name("JohnDoe", "Sample") |> TestRepo.one!()
    end
  end

  def new_user_params(username) do
    %{
      "application_id" => "5dd7c210-3342-477b-ade9-d89b3255e1e9",
      "is_anonymous" => false,
      "mobile_alias" => "",
      "user_name" => username,
      "membership" => %{
        "email" => "sample@emailaddress.com",
        "plaintext_password" => "sample-password",
        "is_approved" => true,
        "is_locked_out" => false
      },
      "profile" => %{
        "PostalCode" => "71111",
        "AgreedToTerms" => "True",
        "AgreedToTermsPhoto" => "False",
        "SilkImageGuid" => "82a6f74e-9ac9-40ce-87c2-3534f78ce27d",
        "LastIPAddress" => "10.4.0.245",
        "LastName" => "Doe",
        "BirthDate" =>
          "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<dateTime>1969-10-05T00:00:00</dateTime>",
        "FirstName" => "John",
        "Sex" => "M"
      }
    }
  end
end
