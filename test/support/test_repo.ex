defmodule TestRepo do
  @moduledoc false

  use Ecto.Repo,
    otp_app: :sql_membership_provider,
    adapter: Ecto.Adapters.Tds

  def init(_type, config) do
    {:ok,
     Keyword.merge(config,
       database: "aspnetdb",
       hostname: "localhost",
       password: "yourStrong(!)Password",
       port: 1433,
       pool: Ecto.Adapters.SQL.Sandbox,
       show_sensitive_data_on_connection_error: true,
       username: "sa"
     )}
  end
end
