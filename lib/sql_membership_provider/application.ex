defmodule SqlMembershipProvider.Application do
  @moduledoc """
  Struct for representing an application.
  """

  use TypedEctoSchema
  import Ecto.Query, only: [from: 2]

  @primary_key {:application_id, :binary_id, autogenerate: true}
  @field_source_mapper fn name ->
    name |> Atom.to_string() |> Macro.camelize() |> String.to_atom()
  end

  typed_schema "aspnet_Applications" do
    field(:application_name, :string)
    field(:lowered_application_name, :string)
    field(:description, :string)

    has_many(:users, SqlMembershipProvider.User, foreign_key: :application_id)
  end

  @doc """
  Fetch an application by ID.
  """
  @spec find_by_application_id(String.t()) :: Ecto.Query.t()
  def find_by_application_id(application_id) when is_binary(application_id) do
    from(a in SqlMembershipProvider.Application,
      where: a.application_id == ^application_id
    )
  end

  @doc """
  Fetch an application by case-insensitive application name.
  """
  @spec find_by_application_name(String.t()) :: Ecto.Query.t()
  def find_by_application_name(application_name) when is_binary(application_name) do
    lowered_application_name = String.downcase(application_name)

    from(a in SqlMembershipProvider.Application,
      where: a.lowered_application_name == ^lowered_application_name
    )
  end
end
