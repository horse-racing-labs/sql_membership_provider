defmodule SqlMembershipProvider.Role do
  @moduledoc """
  Struct for representing a security role that a user may assume
  """

  use TypedEctoSchema
  import Ecto.Query, only: [from: 2]

  @primary_key {:role_id, :binary_id, autogenerate: true}
  @field_source_mapper fn name ->
    name |> Atom.to_string() |> Macro.camelize() |> String.to_atom()
  end

  typed_schema "aspnet_Roles" do
    field(:application_id, :binary_id)
    field(:role_name, :string)
    field(:lowered_role_name, :string)
    field(:description, :string)

    many_to_many(:users, SqlMembershipProvider.User,
      join_keys: [RoleId: :role_id, UserId: :user_id],
      join_through: "aspnet_UsersInRoles"
    )
  end

  @doc """
  Fetch all roles that a user belongs to.
  """
  @spec find_by_user_id(String.t()) :: Ecto.Query.t()
  def find_by_user_id(user_id) when is_binary(user_id) do
    from(r in SqlMembershipProvider.Role,
      join: u in assoc(r, :users),
      where: u.user_id == ^user_id
    )
  end
end
