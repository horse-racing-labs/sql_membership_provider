defmodule SqlMembershipProvider.User do
  @moduledoc """
  Struct for representing a user.
  """

  use TypedEctoSchema
  import Ecto.Query, only: [from: 2]
  import Ecto.Changeset

  @primary_key {:user_id, :binary_id, autogenerate: true}
  @field_source_mapper fn name ->
    name |> Atom.to_string() |> Macro.camelize() |> String.to_atom()
  end

  typed_schema "aspnet_Users" do
    field(:is_anonymous, :boolean)
    field(:last_activity_date, :utc_datetime)
    field(:lowered_user_name, :string)
    field(:mobile_alias, :string)
    field(:user_name, :string)

    belongs_to(:application, SqlMembershipProvider.Application,
      references: :application_id,
      type: :binary_id
    )

    has_one(:profile, SqlMembershipProvider.Profile, foreign_key: :user_id)
    has_one(:membership, SqlMembershipProvider.Membership, foreign_key: :user_id)

    many_to_many(:roles, SqlMembershipProvider.Role,
      join_keys: [UserId: :user_id, RoleId: :role_id],
      join_through: "aspnet_UsersInRoles"
    )
  end

  @doc """
  Changeset for user registration
  """
  @spec create_changeset(SqlMembershipProvider.User.t(), %{atom() => any()}) :: Ecto.Changeset.t()
  def create_changeset(model, params) do
    application_id = params["application_id"]

    merged_params =
      Map.merge(
        params,
        %{"membership" => Map.merge(params["membership"], %{"application_id" => application_id})}
      )

    model
    |> cast(merged_params, [
      :application_id,
      :is_anonymous,
      :mobile_alias,
      :user_name
    ])
    |> validate_required([
      :application_id,
      :is_anonymous,
      :user_name
    ])
    |> put_lowered_user_name(params)
    |> put_change(:last_activity_date, DateTime.truncate(DateTime.utc_now(), :second))
    |> cast_assoc(:membership, with: &SqlMembershipProvider.Membership.create_changeset/2)
    |> cast_assoc(:profile, with: &SqlMembershipProvider.Profile.create_changeset/2)
  end

  defp put_lowered_user_name(changeset, %{"user_name" => username}) do
    changeset
    |> put_change(:lowered_user_name, String.downcase(username))
  end

  @doc """
  Fetch a user by user ID.
  """
  @spec find_by_user_id(String.t()) :: Ecto.Query.t()
  def find_by_user_id(user_id) when is_binary(user_id) do
    from(u in SqlMembershipProvider.User,
      where: u.user_id == ^user_id,
      preload: [:application, :membership, :profile, :roles]
    )
  end

  @doc """
  Fetch a user by case-insensitive username and case-insensitive application name.
  """
  @spec find_by_user_name(String.t(), String.t()) :: Ecto.Query.t()
  def find_by_user_name(user_name, application_name)
      when is_binary(user_name) and is_binary(application_name) do
    lowered_user_name = String.downcase(user_name)
    lowered_application_name = String.downcase(application_name)

    from(u in SqlMembershipProvider.User,
      join: a in assoc(u, :application),
      where: u.lowered_user_name == ^lowered_user_name,
      where: a.lowered_application_name == ^lowered_application_name,
      preload: [:application, :membership, :profile, :roles]
    )
  end
end
