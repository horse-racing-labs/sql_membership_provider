defmodule SqlMembershipProvider.UnsupportedPasswordFormatError do
  defexception [:message, :password_format]

  @impl true
  def exception(value) do
    msg = "Unsupported password format: #{inspect(value)}"
    %SqlMembershipProvider.UnsupportedPasswordFormatError{message: msg}
  end
end
