#!/bin/bash

pass='yourStrong(!)Password'

docker-compose exec sql_server /opt/mssql-tools/bin/sqlcmd -U sa -P $pass -i /code/membership.sql

docker-compose exec sql_server /opt/mssql-tools/bin/sqlcmd -d aspnetdb -U sa -P $pass -i /code/insert-sample-users.sql
