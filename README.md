# SqlMembershipProvider

[Hex.pm](https://hex.pm/packages/sql_membership_provider) [Documentation](https://hexdocs.pm/sql_membership_provider/)

Read and authenticate against ASP.NET Membership Microsoft SQL Server databases.

Do you have an ASP.NET app that uses the built-in Membership Provider that
stores users, passwords, roles, and profiles in a Microsoft SQL Server database?

This library lets your Elixir code talk to a Membership Provider database to lookup users, their profile information roles they belong to, and authenticate their passwords.

This library uses [Tds](https://hex.pm/packages/tds) and [Ecto](https://hex.pm/packages/ecto) to connect to a Microsoft SQL Server database.

## Usage

First you must define an [Ecto.Repo](https://hexdocs.pm/ecto/Ecto.Repo.html) to connect to the MSSQL database:

```Elixir
defmodule MyApp.Repo do
  use Ecto.Repo,
    otp_app: :my_app,
    adapter: Ecto.Adapters.Tds
end
```

Make sure to configure your database credentials:

```elixir
config :my_app, MyApp.Repo,
  database: "aspnetdb",
  hostname: "localhost",
  password: "password",
  port: 1433,
  username: "username"
```

Now you can query for users:

```elixir
iex(1)> user = SqlMembershipProvider.User.find_by_user_name("JohnDoe", "MyApp")
%SqlMembershipProvider.User{
  ...
}

iex(2)> membership = SqlMembershipProvider.Membership.find_by_user_id(user.user_id)
%SqlMembershipProvider.Membership{
  ...
}

iex(3)> SqlMembershipProvider.Membership.is_password_valid?(membership, "password!")
true

iex(4)> profile = SqlMembershipProvider.Profile.find_by_user_id(user.user_id)
%SqlMembershipProvider.Profile{
  ...
}

iex(5)> SqlMembershipProvider.Profile.properties(profile)
%{
  "FirstName" => "John",
  "LastName" => "Doe",
  ...
}

iex(6)> roles = SqlMembershipProvider.Role.find_by_user_id(user.user_id)
[
  %SqlMembershipProvider.Role{
    role_name: "Administrators",
    ...
  }
]
```

## Running Tests

Clone the repo and fetch its dependencies:

```
$ git clone https://gitlab.com/horse-racing-labs/sql_membership_provider.git
$ cd sql_membership_provider
$ mix deps.get
```

[Docker Compose](https://docs.docker.com/compose/install/) is required to run a
sample SQL Server database to run integration tests against.

```
$ docker-compose up
$ ./setup-test-database.sh
$ mix test
```

## Future Work

There are tables remaining to be mapped to schemas. Functions to update data
like the password and profile properties still need to be implemented.

## Help Wanted

* Only the SHA1 password format is supported. Support should be added for [the
  remaining password formats](https://docs.microsoft.com/en-us/dotnet/api/system.web.security.membershippasswordformat?view=netframework-4.8).
* Add support for reading binary properties from profiles
