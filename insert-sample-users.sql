DELETE FROM aspnet_UsersInRoles
GO
DELETE FROM aspnet_Roles
GO
DELETE FROM aspnet_Profile
GO
DELETE FROM aspnet_Membership
GO
DELETE FROM aspnet_Users
GO
DELETE FROM aspnet_Applications
GO

INSERT INTO aspnet_Applications (
  ApplicationName,
  LoweredApplicationName,
  ApplicationId
) VALUES (
  'Sample',
  'sample',
  '5dd7c210-3342-477b-ade9-d89b3255e1e9'
)
GO

INSERT INTO aspnet_Users (
  ApplicationId,
  UserId,
  UserName,
  LoweredUserName,
  IsAnonymous,
  LastActivityDate
) VALUES (
  '5dd7c210-3342-477b-ade9-d89b3255e1e9',
  '1557b0fd-40ad-4c6e-9651-161136ad68d2',
  'JohnDoe',
  'johndoe',
  0,
  GETDATE()
)
GO

INSERT INTO aspnet_Users (
  ApplicationId,
  UserId,
  UserName,
  LoweredUserName,
  IsAnonymous,
  LastActivityDate
) VALUES (
  '5dd7c210-3342-477b-ade9-d89b3255e1e9',
  '34511dc0-9051-451a-b9f1-c9fabdac0e49',
  'Sample-No-Membership',
  'sample-no-membership',
  0,
  GETDATE()
)
GO

INSERT INTO aspnet_Membership (
  ApplicationId,
  UserId,
  Password,
  PasswordFormat,
  PasswordSalt,
  Email,
  LoweredEmail,
  IsApproved,
  IsLockedOut,
  CreateDate,
  LastLoginDate,
  LastPasswordChangedDate,
  LastLockoutDate,
  FailedPasswordAttemptCount,
  FailedPasswordAttemptWindowStart,
  FailedPasswordAnswerAttemptCount,
  FailedPasswordAnswerAttemptWindowStart
) VALUES (
  '5dd7c210-3342-477b-ade9-d89b3255e1e9',
  '1557b0fd-40ad-4c6e-9651-161136ad68d2',
  '4n+nVVpHIrwYQegAAB9HROz01Bg=',
  1,
  'UX3qecrwZlIhkjlOTMu3dQ==',
  'johndoe@example.com',
  'johndoe@example.com',
  1,
  0,
  GETDATE(),
  GETDATE(),
  GETDATE(),
  GETDATE(),
  0,
  GETDATE(),
  0,
  GETDATE()
)
GO

INSERT INTO aspnet_Profile (
  UserId,
  PropertyNames,
  PropertyValuesString,
  PropertyValuesBinary,
  LastUpdatedDate
) VALUES (
  '1557b0fd-40ad-4c6e-9651-161136ad68d2',
  'PostalCode:S:0:5:AgreedToTerms:S:5:4:AgreedToTermsPhoto:S:9:5:SilkImageGuid:S:14:36:LastIPAddress:S:50:10:LastName:S:60:3:BirthDate:S:63:81:FirstName:S:144:4:Sex:S:148:1:',
  '71111TrueFalse82a6f74e-9ac9-40ce-87c2-3534f78ce27d10.4.0.245Doe<?xml version="1.0" encoding="utf-16"?>' + CHAR(13) + CHAR(10) + '<dateTime>1969-10-05T00:00:00</dateTime>JohnM',
  0x,
  GETDATE()
)
GO

INSERT INTO aspnet_Roles (
  ApplicationId,
  RoleId,
  RoleName,
  LoweredRoleName
) VALUES (
  '5dd7c210-3342-477b-ade9-d89b3255e1e9',
  'e032bce3-5d07-4e6d-af4d-7b98b650f992',
  'Administrators',
  'administrators'
)
GO

INSERT INTO aspnet_UsersInRoles (
  UserId,
  RoleId
) VALUES (
  '1557b0fd-40ad-4c6e-9651-161136ad68d2',
  'e032bce3-5d07-4e6d-af4d-7b98b650f992'
)
GO
