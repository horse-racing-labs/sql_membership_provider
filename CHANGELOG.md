# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.9.1] - 2024-04-25
### Added
 - User.create_changeset/2 now has cast_assoc for Profile.create_changeset/2

### Fixed
 - Incorrect param parsing for profile.create_changeset/2 

## [0.9.1] - 2024-04-24
### Added
- Profile.create_changeset/2

### Changed
- Updated TDS dependency

## [0.9.0] - 2023-04-28
### Changed
- Updated dependencies

## [0.8.1] - 2021-10-19
### Added
 - credo & dialyzer static analysis tools

### Fixed
 - Incorrect typespecs for map arguments

## [0.8.0] - 2021-10-19
### Changed
 - Updated dependencies

### Removed
 - User.find_by_user_name/1

## [0.7.0] - 2021-09-20
### Added
 - Membership.password_reset_changeset/2

## [0.6.0] - 2021-09-20
### Added
 - Membership.password_update_changeset/2

## [0.5.0] - 2021-09-16
### Added
 - User.create_changeset/2
 - Membership.create_changeset/2
 - These methods are used for creating new registration changesets

## [0.4.2] - 2021-08-05
### Changed
- Updated dependencies

## [0.4.1] - 2021-08-05
### Changed
- Updated mix lockfile

## [0.4.0] - 2021-08-05
### Added 
- Membership.find_by_email/2 that accepts a case insensitive email address and application name

## [0.3.2] - 2020-10-06
### Changed
- Updated dependencies
- Use typed_ecto_schema to reduce boilerplate

## [0.3.1] - 2020-10-06
### Added 
- User.find_by_user_name/2 that accepts an application name. This lets
you distinguish between users with the same username on different applications.

# Deprecated
- User.find_by_user_name/1 which could return multiple users with the same
  username, but different applications. Use User.find_by_user_name/2 instead.

## [0.3.0] - 2020-09-25
### Changed
- Convert library to use Ecto. All structs are now Ecto schemas, and `find_*`
  functions return Ecto.Query which you must pass to a Ecto.Repo to get results.
### Removed
- Removed the SqlMembershipProvider.TdsUtils module because Ecto takes care of
  these things for us.

## [0.2.0] - 2020-09-11
### Changed
- Updated dependencies

## [0.1.2] - 2020-03-11
### Added
- User.find_by_user_id/2

## [0.1.1] - 2019-08-23
### Fixed
- Allow `nil` field values to pass through `TdsUtils.stringify_uuids/2` without error.

## [0.1.0] - 2019-08-09
### Added
- Initial release
